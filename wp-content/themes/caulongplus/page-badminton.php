<?php /* Template Name: Page badminton */ ?>
<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.badminton');

get_footer();