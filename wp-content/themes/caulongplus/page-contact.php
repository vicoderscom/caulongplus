<?php /* Template Name: Page contact */ ?>
<?php 
use NF\View\Facades\View;

get_header();

echo View::render('contact.contact');

get_footer();
 ?>