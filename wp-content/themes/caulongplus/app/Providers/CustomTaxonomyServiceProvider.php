<?php

namespace App\Providers;

use App\CustomPosts\TaxonomyNews;
use App\CustomPosts\TaxonomyNew_flypower;
use App\CustomPosts\TaxonomyVideo;
use Illuminate\Support\ServiceProvider;

class CustomTaxonomyServiceProvider extends ServiceProvider
{
    /**
     * [$listen description]
     * @var array
     */
    public $listen = [
        TaxonomyNews::class,
        TaxonomyVideo::class
    ];
    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveCustomPost($class);
        }
    }

    /**
     * [resolveCustomPost description]
     * @param  [type] $postType [description]
     * @return [type]           [description]
     */
    public function resolveCustomPost($postType)
    {
        return new $postType();
    }
}
