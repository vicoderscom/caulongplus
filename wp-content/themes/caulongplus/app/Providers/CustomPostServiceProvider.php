<?php

namespace App\Providers;

use App\CustomPosts\Testimonials;
use App\CustomPosts\SlidePost;
use App\CustomPosts\News;
use App\CustomPosts\New_flypower;
use App\CustomPosts\Video;
use App\CustomPosts\DirectTransfer;
use Illuminate\Support\ServiceProvider;

class CustomPostServiceProvider extends ServiceProvider
{
    /**
     * [$listen description]
     * @var array
     */
    public $listen = [
        News::class,
        Video::class,
        DirectTransfer::class
    ];

    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveCustomPost($class);
        }
    }

    /**
     * [resolveCustomPost description]
     * @param  [type] $postType [description]
     * @return [type]           [description]
     */
    public function resolveCustomPost($postType)
    {
        return new $postType();
    }
}
