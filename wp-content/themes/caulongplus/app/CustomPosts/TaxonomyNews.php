<?php

namespace App\CustomPosts;
use NF\Abstracts\CustomTaxonomy;

class TaxonomyNews extends CustomTaxonomy
{
    public $post_type = 'news';

    public $taxonomy_type = 'category_news';

    public $args = [
                      'menu_icon' => 'dashicons-format-status',
                      'rewrite' => [
                          'slug' =>'chuyen-muc-tin-tuc',
                      ]
                   ];
}
