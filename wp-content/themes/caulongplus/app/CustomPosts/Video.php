<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Video extends CustomPost
{
    public $type = 'video';

    public $single = 'video';

    public $plural = 'Video';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
