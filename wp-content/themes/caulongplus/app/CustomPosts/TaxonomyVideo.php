<?php

namespace App\CustomPosts;
use NF\Abstracts\CustomTaxonomy;

class TaxonomyVideo extends CustomTaxonomy
{
    public $post_type = 'video';

    public $taxonomy_type = 'category_video';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}