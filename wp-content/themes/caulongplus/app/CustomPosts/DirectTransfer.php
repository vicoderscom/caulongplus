<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class DirectTransfer extends CustomPost
{
    public $type = 'direct_transfer';

    public $single = 'direct_transfer';

    public $plural = 'Direct transfer';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
