<?php 
class footer_contact extends WP_Widget {
	function __construct() {
    	parent::__construct(
            'Contact_online',
            'Contact_online',
            array( 'description'  =>  'Hỗ trợ trực tuyến' )
        );
    }
    function widget( $args, $instance ) {
    }
}
function create_contact_online_widget() {
    register_widget('footer_contact');
}
add_action( 'widgets_init', 'create_contact_online_widget' );

/*
* widget menu sidebar
*/
class menu_sidebar_1 extends WP_Widget {
    function __construct() {
        parent::__construct(
            'menu_sidebar_1',
            'Menu Sidebar',
            array( 'description'  =>  'menu_sidebar_1' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text1' => '',
            'text2' => '',
            'text3' => '',
            'text4' => '',
            'text5' => '',
            'text6' => '',
            'text7' => '',
            'text8' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text1 = esc_attr($instance['text1']);
        $text2 = esc_attr($instance['text2']);
        $text3 = esc_attr($instance['text3']);
        $text4 = esc_attr($instance['text4']);
        $text5 = esc_attr($instance['text5']);
        $text6 = esc_attr($instance['text6']);

        $text7 = esc_attr($instance['text7']);
        $text8 = esc_attr($instance['text8']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';

        echo '<p>Menu 1:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';
        echo '<p>link menu 1:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';

        echo '<p>Menu 2:<input type="text" class="widefat" name="'.$this->get_field_name('text3').'" value="'.$text3.'" /></p>';
        echo '<p>Link menu 2:<input type="text" class="widefat" name="'.$this->get_field_name('text4').'" value="'.$text4.'" /></p>';

        echo '<p>Menu 3:<input type="text" class="widefat" name="'.$this->get_field_name('text5').'" value="'.$text5.'" /></p>';
        echo '<p>Link menu 3:<input type="text" class="widefat" name="'.$this->get_field_name('text6').'" value="'.$text6.'" /></p>';

        echo '<p>Menu 4:<input type="text" class="widefat" name="'.$this->get_field_name('text7').'" value="'.$text7.'" /></p>';

        echo '<p>Link menu 4:<input type="text" class="widefat" name="'.$this->get_field_name('text8').'" value="'.$text8.'" /></p>';

    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['text2'] = ($new_instance['text2']);
        $instance['text3'] = ($new_instance['text3']);
        $instance['text4'] = ($new_instance['text4']);
        $instance['text5'] = ($new_instance['text5']);

        $instance['text6'] = ($new_instance['text6']);

        $instance['text7'] = ($new_instance['text7']);
        $instance['text8'] = ($new_instance['text8']);

        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text1 = $instance['text1'];
        $text2 = $instance['text2'];
        $text3 = $instance['text3'];
        $text4 = $instance['text4'];
        $text5 = $instance['text5'];

        $text6 = $instance['text6'];

        $text7 = $instance['text7'];

        $text8 = $instance['text8'];

        echo $before_widget;
        ?>
            <div class="slibar-1">
                <p class="title-sidebar"><?php echo $title; ?></p>
                <ul>
                    <li>
                        <i class="fa fa-taxi" aria-hidden="true"></i><a href="<?php echo $text2; ?>"><?php echo $text1; ?></a>
                    </li>

                    <li>
                        <i class="fa fa-credit-card" aria-hidden="true"></i><a href="<?php echo $text4; ?>"><?php echo $text3; ?></a>
                    </li>

                    <li>
                        <i class="fa fa-check" aria-hidden="true"></i><a href="<?php echo $text6; ?>"><?php echo $text5; ?></a>
                    </li>

                    <li>
                        <i class="fa fa-wrench" aria-hidden="true"></i><a href="<?php echo $text8; ?>"><?php echo $text7; ?></a>
                    </li>
                </ul>
            </div>
        <?php
        echo $after_widget;
    }
}
function sidebar_1() {
    register_widget('menu_sidebar_1');
}
add_action( 'widgets_init', 'sidebar_1' );


/*
* widget hotline
*/

class hotline_sidebar extends WP_Widget {
    function __construct() {
        parent::__construct(
            'hotline_sidebar',
            'Hotline Sidebar',
            array( 'description'  =>  'hotline_sidebar' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text1' => '',
            'text2' => '',
            'text3' => '',
            'text4' => '',
            'text5' => '',
            'text6' => '',

        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text1 = esc_attr($instance['text1']);
        $text2 = esc_attr($instance['text2']);
        $text3 = esc_attr($instance['text3']);
        $text4 = esc_attr($instance['text4']);
        $text5 = esc_attr($instance['text5']);
        $text6 = esc_attr($instance['text6']);


        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';

        echo '<p>Khu Vực 1:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';
        echo '<p>Số Điện Thoại Khu Vực 1:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';

        echo '<p>Khu Vực 2:<input type="text" class="widefat" name="'.$this->get_field_name('text3').'" value="'.$text3.'" /></p>';
        echo '<p>Số Điện Thoại Khu Vực 2:<input type="text" class="widefat" name="'.$this->get_field_name('text4').'" value="'.$text4.'" /></p>';

        echo '<p>Sô Điện Thoại Online:<input type="text" class="widefat" name="'.$this->get_field_name('text5').'" value="'.$text5.'" /></p>';
        echo '<p>Số điện thoại:<input type="text" class="widefat" name="'.$this->get_field_name('text6').'" value="'.$text6.'" /></p>';

    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['text2'] = ($new_instance['text2']);
        $instance['text3'] = ($new_instance['text3']);
        $instance['text4'] = ($new_instance['text4']);
        $instance['text5'] = ($new_instance['text5']);

        $instance['text6'] = ($new_instance['text6']);

        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text1 = $instance['text1'];
        $text2 = $instance['text2'];
        $text3 = $instance['text3'];
        $text4 = $instance['text4'];
        $text5 = $instance['text5'];

        $text6 = $instance['text6'];

        echo $before_widget;
        ?>
            <div class="sidebar-2">
                <p class="title-sidebar"><?php echo $title; ?></p>

                <div class="hotline">
                    <ul>
                        <div class="img_hotline">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/sidebar/hotline.png" alt="">
                        </div>

                        <li class="title_phone">
                            <i class="fa fa-phone-square" aria-hidden="true"></i><a href="tel:<?php echo $text2; ?>"><?php echo $text1;?></a>
                        </li>

                        <li>
                            <a class="call-sidebar" href="tel:<?php echo $text2; ?>"><?php echo $text2; ?></a>
                        </li>

                        <li class="title_phone">
                            <i class="fa fa-phone-square" aria-hidden="true"></i><a href="tel:<?php echo $text4; ?>"><?php echo $text3; ?></a>
                        </li>

                        <li>
                            <a class="call-sidebar" href="tel:<?php echo $text4; ?>"><?php echo $text4; ?></a>
                        </li>

                        <li>
                            <i class="fa fa-phone-square" aria-hidden="true"></i><a href="tel:<?php echo $text6; ?>"><?php echo $text5; ?></a>
                        </li>

                        <li>
                            <a class="call-sidebar" href="tel:<?php echo $text6; ?>"><?php echo $text6; ?></a>
                        </li>
                    </ul>
                </div>

            </div>
        <?php
        echo $after_widget;
    }
}
function sidebar_2() {
    register_widget('hotline_sidebar');
}
add_action( 'widgets_init', 'sidebar_2' );

/*
* widget page contact
*/

class thong_tin_contact_page extends WP_Widget {
    function __construct() {
        parent::__construct(
            'thong_tin_contact_page',
            'Thông Tin Trang Contact',
            array( 'description'  =>  'thong_tin_contact_page' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text1' => '',
            'text2' => '',
            'text3' => '',
            'text4' => '',
            'text5' => '',
            'text6' => '',

        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text1 = esc_attr($instance['text1']);
        $text2 = esc_attr($instance['text2']);
        $text3 = esc_attr($instance['text3']);
        $text4 = esc_attr($instance['text4']);
        $text5 = esc_attr($instance['text5']);
        $text6 = esc_attr($instance['text6']);


        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';

        echo '<p>Miền Bắc:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';
        echo '<p>Số Điện Thoại Miền Bắc:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';

        echo '<p>Miền Trung - Nam:<input type="text" class="widefat" name="'.$this->get_field_name('text3').'" value="'.$text3.'" /></p>';
        echo '<p>Số Điện Thoại Miền Trung - Nam:<input type="text" class="widefat" name="'.$this->get_field_name('text4').'" value="'.$text4.'" /></p>';

        echo '<p>Email:<input type="text" class="widefat" name="'.$this->get_field_name('text5').'" value="'.$text5.'" /></p>';
        echo '<p>Website:<input type="text" class="widefat" name="'.$this->get_field_name('text6').'" value="'.$text6.'" /></p>';

    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['text2'] = ($new_instance['text2']);
        $instance['text3'] = ($new_instance['text3']);
        $instance['text4'] = ($new_instance['text4']);
        $instance['text5'] = ($new_instance['text5']);

        $instance['text6'] = ($new_instance['text6']);

        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text1 = $instance['text1'];
        $text2 = $instance['text2'];
        $text3 = $instance['text3'];
        $text4 = $instance['text4'];
        $text5 = $instance['text5'];

        $text6 = $instance['text6'];

        echo $before_widget;
        ?>
            <div class="content-contact">
                <h3><?php echo $title; ?></h3>
                <p>Miền Bắc: <?php echo $text1; ?></p>
                <p>Điên Thoại: <?php echo $text2; ?></p>
                <p>Miền Trung - Nam: <?php echo $text3; ?></p>

                <p>Điện Thoại: <?php echo $text4; ?></p>
                <p>Email: <?php echo $text5; ?></p>
                <p>Website: <?php echo $text6; ?></p>
            </div>
        <?php
        echo $after_widget;
    }
}
function contact_content() {
    register_widget('thong_tin_contact_page');
}
add_action( 'widgets_init', 'contact_content' );

/*
* contact form
*/

class contact_form extends WP_Widget {
    function __construct() {
        parent::__construct(
            'contact_form',
            'Form Trang Contact',
            array( 'description'  =>  'contact_form' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text1' => '',
            'text2' => '',

        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text1 = esc_attr($instance['text1']);
        $text2 = esc_attr($instance['text2']);
        
        echo '<p>Tiêu Đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';

        echo '<p>Lời Nhắc:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';

        echo '<p>Nhúng short code:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';

    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['text2'] = ($new_instance['text2']);

        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text1 = $instance['text1'];
        $text2 = $instance['text2'];

        echo $before_widget;
        ?>
            <div class="form_contact">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-10 col-xs-12 form_nhap">
                        <p class="title_form"><?php echo $title; ?><br><?php echo $text2; ?></p>
                        <div class="form_thongtin">
                            <?php
                                echo do_shortcode( $text1 );
                            ?>
                        </div>
                    </div>
                </div>
                
            </div>
        <?php
        echo $after_widget;
    }
}
function widget_form_contact() {
    register_widget('contact_form');
}
add_action( 'widgets_init', 'widget_form_contact' );