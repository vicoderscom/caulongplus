<?php

namespace App\Widgets;

use MSC\Widget;
/**
* 
*/
class NewsHomepage extends Widget
{
	
	public function __construct()
	{
		$widget = [
		    'id'          => 'news_homepage',
		    'label'       => __('News for Homepage', 'caulongplus'),
		    'description' => 'This widget shows news on homepage'
		];

		$fields = [
			[
		        'label' => __('Title widget', 'caulongplus'),
		        'name'  => 'title_widget',
		        'type'  => 'text',
			],
			[
		        'label' => __('Slug Category', 'caulongplus'),
		        'name'  => 'slug_category',
		        'type'  => 'text',
			],
			[
		        'label' => __('Link more', 'caulongplus'),
		        'name'  => 'link_more',
		        'type'  => 'text',
			]
		];

		parent::__construct($widget, $fields);
	}

	/**
	 * [inherit]
	 */
	public function handle($instance)
	{
		?>
		<div class="list_item list_product">
            <h3 class="title_news_home"><?php echo ((!empty($instance['title_widget'])) ? $instance['title_widget'] : 'TIN TỨC - VIDEO CẦU LÔNG'); ?></h3>
            <ul class="col-md-12 col-sm-12 col-xs-12 list_news">
                <?php
                    $list_news = [
                        'post_type'=>'news',
                        'posts_per_page' => 6,
                        'post_status'    => 'publish',
                        'tax_query' => [
                            [
                               'taxonomy' =>'category_news',
                               'field' => 'slug',
                               'terms' => (!empty($instance['slug_category']) ? $instance['slug_category'] : 'tin-tuc-trang-chu')
                            ]
                        ],
                    ];
                    $get_news = new \WP_Query($list_news);
                    if($get_news->have_posts()) : 
                        while($get_news->have_posts()) : $get_news->the_post(); 
                        if(has_post_thumbnail()) {
                            $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                        } else {
                            $img = get_stylesheet_directory_uri() . '/resources/assets/images/no_image.svg';
                        }
                        $link = get_permalink($post->ID);
                        ?>
                            <li class="row">
                                <div class="col-md-3 col-sm-6 col-xs-4">
                                    <div>
                                        <div class="img">
                                            <a href=" <?php echo $link ?>">
                                                <img style="background-image: url(<?php echo $img ?>);" src="<?php echo get_template_directory_uri() ?>/resources/assets/images/product.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-6 col-xs-8 news_desc">
                                    <div>
                                        <div class="desc">
                                            <h3>
                                                <a href="<?php echo $link ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h3>
                                            <span><?php echo (get_post_time( 'l, d-F-Y', false, $post->ID, 'vi' )) ?></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                        endwhile;
                    endif;
                ?>
                <span class="news_more">
                    <a href="<?php echo (!empty($instance['link_more']) ? $instance['link_more'] : site_url('tin-tuc-cau-long')); ?>">Xem tiếp >></a>
                </span>

                <?php //var_dump(site_url()); ?>
            </ul>
        </div>
		<?php
	}
}