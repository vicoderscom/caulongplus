<?php /* Template Name: Page products */ ?>
<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.list_product');

get_footer();