<?php /* Template Name: Page introduce */ ?>
<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.introduce');

get_footer();