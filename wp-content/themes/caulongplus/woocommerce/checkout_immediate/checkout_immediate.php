<div class="checkout_payment_immediate">
	<h3 class="title"><i style="color: #363e8b;margin-right: 10px;" class="fa fa-credit-card" aria-hidden="true"></i>Chuyển khoản trực tiếp</h3>
	<p>Khách hàng có thể chuyển khoản qua các tài khoản dưới đây, ngay sau khi nhận được tiền chuyển khoản, Sieuthicaulong sẽ gọi điện cho quý khách để xác nhận địa chỉ và tiến hành gửi hàng</p>
	<div class="content">
	    <?php 
	        $direct_transer= new WP_Query(array(
                'post_type'=>'direct_transfer',
            ));
            if($direct_transer->have_posts()) : 
            	while($direct_transer->have_posts()): $direct_transer->the_post();
                $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                ?>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="item">
						    <div class="img col-md-4 col-sm-12">
						        <img src="<?php echo $img; ?>" alt="">
						    </div>
					    	<div class="desc col-md-8 col-sm-12">
					    		<h3><?php the_title(); ?></h3>
					    		<p>Chủ tài khoản: <?php echo get_field('chu_tai_khoan') ?></p>
					    		<p>Số tài khoản: <?php echo get_field('so_tai_khoan') ?></p>
					    		<p>Chi nhánh: <?php echo get_field('chi_nhanh') ?></p>
					    	</div>
						</div>
					</div>
                <?php
                endwhile;
            endif;
	     ?>
	</div>
</div>
