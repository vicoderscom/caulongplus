<div class="search">
    <div class="input-group search_form">
        <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <select name="" id="">
                    <option value="">Tất cả</option>
                    <option value="">Kinh te</option>
                    <option value="">Van Hoa</option>
                    <option value="">Chinh tri</option>
                </select>
            <span class="caret"></span></button>
        </div>
        <form action="<?php echo esc_url(home_url('/')); ?>" role="search" method="get" id="searchform" class="searchform msc-form">
            <input type="hidden" name="search" value="product">
            <input type="text" id="s" value="" name="s" class="form-control" aria-label="..." placeholder="Nhập từ khoá cần tìm?">
            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
</div>
