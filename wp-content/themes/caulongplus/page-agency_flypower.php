<?php /* Template Name: Page agency flypower */ ?>
<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.agency_flypower');

get_footer();