<?php

use NF\View\Facades\View;

$app = require_once __DIR__ . '/bootstrap/app.php';

include(TEMPLATEPATH . "/bootstrap/font_family.php");
include(TEMPLATEPATH . '/app/Widgets/footer_contact.php');

// custom logo
class setting {
	function __construct() {
		add_theme_support( 'custom-logo' );
	}
}
new setting();


if(!function_exists('setup_site')) {
	function setup_site() {
		register_nav_menus( array(
            'primary' => __( 'Menu chính', 'caulongplus' ),
            'service-footer' => __( 'Menu kinh doanh đồ thể  footer', 'caulongplus' ),
            'introduce-sidebar' => __( 'Menu giới thiệu sidebar', 'caulongplus' ),
            'ask-sidebar' => __( 'Menu hỏi đáp sidebar', 'caulongplus' ),
            'guide-sidebar' => __( 'Menu hướng dẫn sidebar', 'caulongplus' ),
        ) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
	}
	add_action('init', 'setup_site');
}


if (!function_exists('widget_register')) {
	function widget_register() {
		$sidebars = [
			[
				'name'          => __('Footer liên hệ', 'footer_contact'),
				'id'            => 'footer_contact',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Footer fanpage facebook', 'footer_fanpage_facebook'),
				'id'            => 'footer_fanpage_facebook',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],

			[
				'name'          => __('Menu Sidebar 1', 'menu_sidebar'),
				'id'            => 'menu_sidebar_1',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],

			[
				'name'          => __('Liên Hệ hotline', 'hotline_sidebar'),
				'id'            => 'hotline_sidebar_2',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],

			[
				'name'          => __('Trang Contact', 'contact_page_sidebar'),
				'id'            => 'contact_page',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Menu Footer', 'menu_footer'),
				'id'            => 'menu_footer_one',
				'description'   => __('This is sidebar for header text', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('News Homepage', 'caulongplus'),
				'id'            => 'news_homepage_widget',
				'description'   => __('This is sidebar for news widget on homepage', 'caulongplus'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]
		];
		foreach ($sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}
	add_action('widgets_init', 'widget_register');
}


function get_the_popular_excerpt($limit){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
	$excerpt = $excerpt. ' [...]';
	return $excerpt;
}
add_filter('woocommerce_cart_needs_payment', '__return_false');


add_action( 'wp_ajax_add_foobar', 'prefix_ajax_add_foobar' );
add_action( 'wp_ajax_nopriv_add_foobar', 'prefix_ajax_add_foobar' );

function prefix_ajax_add_foobar() {
   $CART = WC()->cart;
   $quantity = $_POST['quantity'];
   $product_id = $_POST['product_id'];
   $collect = $_POST['collect'];
   $collect = json_decode($collect);
   if(isset($collect) && !empty($collect)) {
	   foreach($collect as $collect_val) {
		   if($CART->add_to_cart($product_id, $collect_val[0], $collect_val[1])) {
		   	  echo 'yesss';
		   } else {
		   	  echo 'no';
		   }
	   }
   } else {
	   echo 'no';
   }
}

add_filter('site_transient_update_plugins', '__return_false');

function add_theme_option() {
	add_menu_page(
		'theme_option',
		'Theme option',
		'manage_options',
		'theme_option',
		'show_theme_option',
		'',
		'2'
	);
};

function show_theme_option() {
	if(isset($_POST['save_theme_option'])) {
		update_option('font_family', $_POST['font_family']);
	}
	$font_family = get_option('font_family'); 
	include(TEMPLATEPATH . "/bootstrap/theme_option.php");
}
add_action('admin_menu', 'add_theme_option');


add_action('wp_ajax_my_action', 'data_ajax');
add_action('wp_ajax_nopriv_my_action', 'data_ajax');
include(TEMPLATEPATH . "/bootstrap/sort_product.php");


