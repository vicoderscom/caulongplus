<?php

namespace NF\Interfaces;

/**
 * create an interface for custom taxonomy
 *
 * @author Van Quang
 * @since  1.0.0
 */
interface CustomTaxonomyInterface
{
    public function __construct();

    public function getArgs();

    public function registerTaxonomy();
}
