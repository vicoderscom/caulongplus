<?php

namespace NF\Abstracts;

use NF\Interfaces\CustomPostInterface;

class Widget implements CustomPostInterface
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        add_action('init', [$this, 'registerPostType']);
    }

    /**
     * [registerPostType description]
     * @return [type] [description]
     */
    public function registerPostType()
    {
        $result = register_post_type($this->type, $this->getArgs());

        if (is_wp_error($result)) {
            wp_die($result->get_error_message());
        };
    }

    /**
     * [getArgs description]
     * @return [type] [description]
     */
    public function getArgs()
    {
        $labels = array(
            'name'          => __('Precious herbs', 'precious_herbs'),
            'id'            => 'precious_herbs',
            'description'   => __('This is all section on Precious herbs', 'thaoduoc'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>'
        );

        $this->args = wp_parse_args($this->args, $defaults);

        $this->args['labels'] = wp_parse_args($this->args['labels'], $labels);

        return $this->args;
    }
}
