<?php

namespace NF\Abstracts;

use NF\Interfaces\CustomTaxonomyInterface;

class CustomTaxonomy implements CustomTaxonomyInterface
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        add_action('init', [$this, 'registerTaxonomy']);
    }

    /**
     * [registerPostType description]
     * @return [type] [description]
     */
    public function registerTaxonomy()
    {
        $result = register_taxonomy($this->taxonomy_type, $this->post_type, $this->getArgs());

        if (is_wp_error($result)) {
            wp_die($result->get_error_message());
        };
    }

    /**
     * [getArgs description]
     * @return [type] [description]
     */
    public function getArgs()
    {
        $labels = array(
            'label' => $this->taxonomy_type,
            'hierarchical' => true,
        );
        $this->args = wp_parse_args($this->args, $labels); 

        return $this->args;
    }
}
