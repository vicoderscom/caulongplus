<div class="container video_detail">
	<div class="row">
		
		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>

		<div class="col-md-9 col-sm-7 list-item">

			@php
				if(have_posts()){
					while (have_posts()) :
						the_post();
			@endphp


					<div>
						<h3>
							@php
								the_title();
							@endphp
						</h3>
					</div>

					<div class="detial_content_video">
						@php
							the_content();
						@endphp
					</div>

					<div style="margin-top: 15px;">
						<div class="fb-like" data-href="https://www.facebook.com/Sieuthicaulong.vn/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
						<!-- Place this tag where you want the +1 button to render. -->
						<div class="g-plusone" data-size="medium" data-annotation="none"></div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>

					<div class="row">
						<div class="col-md-12">
							<p class="title_involve">video Liên quan</p>
							<ul class="row video_involve">

								@php
									$video_lien_quan = array(
										'post_type' => 'video',
										'posts_per_page' =>4,
									);

									$lien_quan = new WP_Query($video_lien_quan);

										if($lien_quan->have_posts()){
											while ( $lien_quan->have_posts() ) : $lien_quan->the_post();

											$img_video_detail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
		                					$link_video_detail = get_permalink($post->ID);
								@endphp

										<li class="item_video col-md-3">
											<div class="img_video">
												<a href="{{ $link_video }}"><img class="image" style="background-image: url({{ $img_video_detail }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/video/video.png" alt=""></a>
											
												<a href="{{ $link_video }}"><img class="icon_video" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/video/youtube-icon.png" alt=""></a>
											</div>
											
											<p class="title_post_video"><a href="{{ $link_video }}">@php the_title(); @endphp</a></p>
							
										</li>

									@php
											endwhile;
										}
									@endphp
							</ul>
						</div>
					</div>

			@php
					endwhile;
				}
			@endphp
		</div>

		<div class="col-md-3 col-sm-5 sidebar-page">

			@php
				dynamic_sidebar('menu_sidebar_1');
			@endphp

			@php
				dynamic_sidebar('hotline_sidebar_2');
			@endphp

			@include( 'sidebar/sidebar_category')


			@include( 'sidebar/sidebar_product_new')

		</div>
	</div>
</div>