<div class="product_detail">
	<div class="container" style="padding:0;">
	    <div class="breadcrumb">
	        <div class="col-md-12">
	            <div class="row">
			        {!! woocommerce_breadcrumb() !!}
		        </div>
		    </div>
	    </div>
	    <div class="col-md-9">
	        <div class="row">
	            <div class="col-md-12">
	            	<div class="row">
	            		<div class="message_add_to_cart col-md-12">
	            			@php 
	            			   do_action( 'woocommerce_before_single_product' ); 
	            			@endphp
	            		</div>
	            	</div>
	            </div>
	            <div class="col-md-12">
	                <div class="row">
					    <div class="col-md-5 col-sm-5 col-xs-12">
					        <div class="images image_product_view">
							@include('detail.inc.image_view')
							</div>
					    </div>
					    <div class="col-md-7 col-sm-7 col-xs-12">
					        <div class="info_fun_product">
					        	<h3 class="title">
					        		@php the_title(); @endphp
					        	</h3>
					        	<ul>
					        		<li>Tình trạng: 
					        		    @if($status_stock)
					        		        {!! 'còn hàng' !!}
					        		    @else 
					        		        {!! 'hết hàng' !!}
					        		    @endif
					        		</li>
					        	</ul>
					        	<div class="price_product_detail">
					        	    <div class="price">
							        	Giá bán: <span>{!! $product->get_price_html() !!}</span>
					        	    </div>
					        	</div>
					        	@if($pro_type)
					        	<div class="table_add_to_cart">
						        	<table class="table">
									    <thead>
										    <tr>
											    <th>Lượng hàng đang có</th>
											    <th>Kích cỡ</th>
											    <th>Số lượng mua</th>
										    </tr>
									    </thead>
									    <tbody>
									        @if(isset($colect_cart))
									            @foreach($colect_cart as $key => $colect_cart_val)
												    <tr>
													    <td class="quantity{{$colect_cart_val['variation_id']}}">
													        <input class="quantity_total_input quantity_total_input{{$key}}" 
													               type="text" value="{!! $colect_cart_val['quantity'] ? $colect_cart_val['quantity'] : 0 !!}"
													    > Chiếc
													    </td>
													    <td><input type="text" value="{!! $colect_cart_val['size'] !!}"></td>
													    <td class="quantity{{$colect_cart_val['variation_id']}}">
													    	<input class="minus" type="button" value="-">
															<input type="text" value="0" placeholder="Nhập số lượng" class="quantity quatity{{$key}}">
															<input class="plus" type="button" value="+">
													    </td>
												    </tr>
												@endforeach
											@endif
									    </tbody>
									</table>
					        	</div>
					        	@endif
					        	<div class="social_share">
					        		<div>
										<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
										<!-- Place this tag where you want the +1 button to render. -->
										<div class="g-plusone" data-size="medium" data-annotation="none"></div>
										<a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
									</div>
					        	</div>
					        	@include('detail.inc.add_to_cart')
					        	@if($pro_type)
						        	<button id="click_add_to_cart" class="btn btn-info">Thêm vào giỏ hàng</button>
					        	@else
						        	<div class="add_cart_simple">
							        	@php
							        		do_action( 'woocommerce_after_shop_loop_item' );
							        	@endphp
						        	</div>
					        	@endif
					        </div>
					    </div>
					</div>
			    </div>
			    <div class="col-md-12">
			        <div class="desc_product_detail">
			    	    <div class="title">
			    	    	<h3>Chi tiết sản phẩm</h3>
			    	    </div>
			    		<div class="description product_description">
			    			@php the_content() @endphp
			    		</div>
			    	</div>
			    </div>
			    <div class="col-md-12 content_home">
			        <div class="container-fluid list_product">
				    	<div class="product_involve">
				    	    <ul>
	    	    			@php
	    	    			    if(isset($info_terms)) {
	                                for($i = 0; $i < 10; $i++) {

	                                	if(has_post_thumbnail($info_terms[$i]->ID)) {
	                                        $img = wp_get_attachment_url(get_post_thumbnail_id($info_terms[$i]->ID));
	                                    } else {
	                                        $img = get_template_directory_uri() . '/resources/assets/images/no_image.svg';
	                                    }
	                                    $link = get_permalink($info_terms[$i]->ID);
	                                    
	                                    $product_variale = new WC_Product_Variable( $info_terms[$i]->ID);
	                                    $sync_stock_status = $product_variale->sync_stock_status($info_terms[$i]->ID);
	                                    $status_stock = $sync_stock_status->stock_status;
	                                    $is_on_sale = $product_variale->is_on_sale($info_terms[$i]->ID);
	                                    $price_html = $product_variale->get_price_html();
	                                    
	                                    $product = new WC_Product($info_terms[$i]->ID); 
	                                    $regular_price = $product->regular_price;
	                                    $price_sale = $product->sale_price;
	                                    $price = $product->price;

	                                    if(empty($price_html)) {
	                                        $price_html = $product->get_price_html();
	                                        if($product->is_on_sale()) {
	                                            $is_on_sale = true;
	                                        } else {
	                                            $is_on_sale = false;
	                                        }
	                                        if($product->get_stock_status() === 'instock') {
	                                            $status_stock = 'instock';
	                                        } else {
	                                            $status_stock = 'outofstock';
	                                        }
	                                    } 
		                                @endphp
		                                <li class="col-md-3 col-sm-4 col-xs-6">
                                            <div class="status_stock">
                                                @if($status_stock === 'instock')
                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/instock.png" alt="">
                                                @else
                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/outstock.png" alt="">
                                                @endif
                                            </div>
                                            <div class="img">
                                                <a href="{{ $link }}">
                                                    <img style="background-image: url({{ $img }});" src="{{ get_template_directory_uri() }}/resources/assets/images/product.png" alt="">
                                                </a>
                                            </div>
                                            @if($is_on_sale)
                                            <div class="mess_sale">
                                                <img src="{{ get_template_directory_uri() }}/resources/assets/images/home/mess_sale.png" alt="">
                                            </div>
                                            @endif
                                            <div class="desc">
                                                <h3>
                                                    <a href="{{ $link }}">{!! $info_terms[$i]->post_title !!}</a>
                                                </h3>
                                                <span class="price">{!! $price_html !!}</span>
                                            </div>
                                        </li>
		                                @php
		                            }
		                        }
	                        @endphp
	                        </ul>
				    	</div>
			        </div>
			    </div>
			</div>

			{{-- 
			========== danh muc ======================= --}}

			<div class="danhmuc">
				<ul class="row">
					@php
						$product_cat = [
			                'taxonomy'=> 'product_cat',
			                'post_type' => 'product',
			                'parent' =>0,
			                'exclude' => 48
			            ];
			            $get_list_cates = get_categories($product_cat);

			            // echo "<pre>";
			            // var_dump($get_list_cates);
			          
					@endphp

					@php
						foreach ($get_list_cates as $value) {

							$cate_link = get_category_link($value->term_id);

							$thumbnail_id = get_woocommerce_term_meta( $value->term_id, 'thumbnail_id', true );
							$images = wp_get_attachment_url( $thumbnail_id );

							// var_dump($images);
							// die();

					@endphp
						<li class="col-md-3">
							<a href="{{ $cate_link }}">
								<img class="images-danhmuc" style="background-image: url({{ $images }});" src="{{ get_template_directory_uri() }}/resources/assets/images/single-danhmuc.png" alt="">

								<div class="title-danhmuc">{{ $value->name }}</div>
							</a>
						</li>
					@php
						}
					@endphp
				</ul>
			</div>
{{-- 
			==================================== --}}
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12">
			<div class="row">

				@php dynamic_sidebar('menu_sidebar_1'); @endphp

			    @php dynamic_sidebar('hotline_sidebar_2'); @endphp

				@include( 'sidebar/sidebar_category')

			    @include( 'sidebar/sidebar_product_new')

			</div>
		</div>
	</div>
</div>