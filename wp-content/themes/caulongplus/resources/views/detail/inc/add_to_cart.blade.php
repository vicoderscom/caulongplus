@php
	global $woocommerce;
	$cart_url = $woocommerce->cart->get_cart_url();
@endphp
<script>
    jQuery(function($){
		function add_to_cart_ajax() {
	    	var convert_array = <?php echo json_encode($variation_id); ?>;
	    	var url_cart = "<?php echo $cart_url; ?>";
    		var url_site = "<?php echo $url_site; ?>"; 
    		var change_quantity = [];
			for(x in convert_array) {
				var element = 'quantity' + convert_array[x];
				var element_val = $('.'+element+' .quantity').val();
				var element_val_total = $('.'+element+ ' .quantity_total_input').val();
				element_val = parseInt(element_val);
				element_val_total = parseInt(element_val_total);
				if(element_val > 0 && element_val_total > 0 && element_val_total >= element_val) {
					change_quantity.push([element_val, convert_array[x]]);
				}
				// console.log(element_val);
				// return false;
			}
			// return false;
			change_quantity = JSON.stringify(change_quantity);
			var data_url = url_site + "/wp-admin/admin-ajax.php";
			var quantity = 5;
			var product_id = <?php echo $product_id; ?>;
			var str = 'action=add_foobar' + '&quantity=' + quantity + '&product_id=' + product_id + '&collect=' + change_quantity;
			try {
				$.ajax({
					type: "POST",
					url: data_url,
					data: str,
					success: function(response) {
						if(response.length > 4) {
    						alert('Bạn đã thêm sản phẩm thành công.');
							window.location.href = url_cart;
						} else {
							alert('Sản phẩm thêm không thành công. Vui lòng xem lại.');
						}
						console.log(response);
						console.log(response.length);
					},
					error: function(error) {
						console.log(error);
					}
				});
			} catch(e) {
				alert('You messed something up');
			}
		}
		$('#click_add_to_cart').click(function(){
			add_to_cart_ajax();
		});
    });
</script>