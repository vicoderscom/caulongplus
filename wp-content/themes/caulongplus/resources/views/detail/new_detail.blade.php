<div class="container chitiet_news">
	<div class="row">
	
		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>

		@php
			if (have_posts()){

			while (have_posts()) {
				the_post();
		@endphp
			<div class="col-md-9 col-sm-12 col-xs-12 conten_news description">
				<h3>
					@php
						the_title();
					@endphp
				</h3>

				<div>
					@php
						the_content();
					@endphp
				</div>
				
				<div class="social_share">
					<div class="fb-like" data-href="https://www.facebook.com/Sieuthicaulong.vn/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
					<!-- Place this tag where you want the +1 button to render. -->
					<div class="g-plusone" data-size="medium" data-annotation="none"></div>
					<a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
				

			<div class="tin_lien_quan">
				<p>Tin Tức Liên Quan</p>

				<ul>
					@php

						$list_lien_quan = array(
							'post_type' => 'news',
							'posts_per_page' =>10,
						);


						$post_lienquan = new WP_Query($list_lien_quan);
						if($post_lienquan->have_posts()){
							while ( $post_lienquan->have_posts() ) : $post_lienquan->the_post();
					@endphp
					
					
						<li>
							<i class="fa fa-square" aria-hidden="true"></i><a href="{{ get_permalink($post->ID) }}">@php the_title(); @endphp</a>
						</li>
					
				
					@php
						endwhile;
						}
						}
						}
					@endphp
				</ul>


				<div class="danhmuc">
					<ul class="row">
						@php
							$product_cat = [
				                'taxonomy'=> 'product_cat',
				                'post_type' => 'product',
				                'parent' =>0,
				                'exclude' => 48
				            ];
				            $get_list_cates = get_categories($product_cat);

				            // echo "<pre>";
				            // var_dump($get_list_cates);
				          
						@endphp

						@php
							foreach ($get_list_cates as $value) {

								$cate_link = get_category_link($value->term_id);

								$thumbnail_id = get_woocommerce_term_meta( $value->term_id, 'thumbnail_id', true );
								$images = wp_get_attachment_url( $thumbnail_id );

								// var_dump($images);
								// die();

						@endphp
							<li class="col-md-3">
								<a href="{{ $cate_link }}">
									<img class="images-danhmuc" style="background-image: url({{ $images }});" src="{{ get_template_directory_uri() }}/resources/assets/images/single-danhmuc.png" alt="">

									<div class="title-danhmuc">{{ $value->name }}</div>
								</a>
							</li>
						@php
							}
						@endphp
					</ul>
				</div>
			</div>
	</div>

	<div class="col-md-3 col-sm-12 col-xs-12">
		<div class="row">

			@php dynamic_sidebar('menu_sidebar_1'); @endphp

		    @php dynamic_sidebar('hotline_sidebar_2'); @endphp

			@include( 'sidebar/sidebar_category')

		    @include( 'sidebar/sidebar_product_new')

		</div>
	</div>
</div>
</div>