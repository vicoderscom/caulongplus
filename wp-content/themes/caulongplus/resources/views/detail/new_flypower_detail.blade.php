<div class="container new_flypower_detail">
	<div class="row">
		
		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>

		<div class="col-md-9 col-sm-7 list-item">
			
		</div>

		<div class="col-md-3 col-sm-5 sidebar-page">

			@php
				dynamic_sidebar('menu_sidebar_1');
			@endphp

			@php
				dynamic_sidebar('hotline_sidebar_2');
			@endphp

			@include( 'sidebar/sidebar_category')


			@include( 'sidebar/sidebar_product_new')

		</div>
	</div>
</div>