@php
    global $product;
    $object_current = get_queried_object();
    if ( $product->is_type( 'variable' ) ) {
    	$pro_type = true;
		$attr_variation = $product->get_variation_attributes();
		foreach($attr_variation as $key => $val) {
			$attr_variation = $val;
		}
	
		$attr_variation_reverse = array_reverse($attr_variation);
		$url_site = get_site_url();
		$product_current = $object_current;
		$product_id = $product_current->ID;

		$variations = $product->get_available_variations(); 
		$variation_id = [];
	    foreach($variations as $variation) {
			$variation_id[] = $variation['variation_id'];
	    }
	    $quantity_variation = [];
	    foreach($variations as $variations_val) {
	    	$quantity_variation[] = $variations_val['stock_quantity'];
	    }
	    $colect_cart = [];
	   	foreach($attr_variation as $key => $attr) {
	   		$child = [
	   			'size'=> $attr,
	   			'quantity'=> $quantity_variation[$key],
	   			'variation_id' => $variation_id[$key]
	   		];
	   		$colect_cart[] = $child;
	   	}
	   	$colect_cart = array_reverse($colect_cart);
    } else {
    	$pro_type = false;
    }
    $status_stock = $product->get_stock_status();
    if($status_stock === 'instock') {
        $status_stock = true;
    } else {
    	$status_stock = false;
    }


    $terms_parents = get_the_terms($object_current->ID, 'product_cat');

    $info_terms = [];
    foreach($terms_parents as $terms_parent) {
    	// var_dump($terms_parent->slug);
    	if($terms_parent->slug != 'san-pham-moi') {
		    $product_cat_1 = [
		        'post_type'=>'product',
		        'posts_per_page' => -1,
		        'post_status'    => 'publish',
		        'orderby'        => 'rand',
		        'tax_query' => [
		            [
		               'taxonomy' =>'product_cat',
		               'field' => 'slug',
		               'terms' => $terms_parent->slug
		            ]
		        ],
		    ];
		    $get_product = new WP_Query($product_cat_1);
		    foreach($get_product->posts as $val_product) {
			    $info_terms[] = $val_product;
		    }
		}
    }
@endphp
@include('detail.inc.detai')








