<div class="sidebar-3">
	<p class="title-sidebar">sản phẩm</p>

		<ul class="list-category">
			@php
				global $product;

				$args_cate = [
					'taxonomy'         => 'product_cat',
					'post_type' => 'product',
					'parent' =>0
				];
				$product_categories = get_categories($args_cate);
			@endphp
			
			@php	
				foreach ($product_categories as $key => $value):

				$name_cte = $value->name;

				$id_category = $value->term_id;
				$link = get_category_link($value->term_id);
			@endphp
				<li id="@php echo $id_category; @endphp" class="category">
					<i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{ $link }}">@php echo $name_cte; @endphp</a>
					<ul class="list-name-product">
					
					@php
						foreach ($product_categories as $key => $value2):

						$id_product = $value2->term_id;
						
						$args = [
							'post_type'      => 'product',
							'posts_per_page' => -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'product_cat',
									'field'    => 'id',
									'terms'    => $value2->term_id,
								),
							),
						];

						$loop = new WP_Query($args);

							if($loop->have_posts() && $id_category == $id_product){
								while ($loop->have_posts()): $loop->the_post();
								$price   = get_post_meta(get_the_ID(), '_regular_price', true);

								$title_product = get_the_title();
								$link_product = get_permalink();
							@endphp
			
							<li id="{{ $id_product }}">
								<a href="{{$link_product}}">@php echo $title_product; @endphp</a>
							</li>

							@php
								endwhile;
							}
						endforeach;
					@endphp
					</ul>
				</li>
			@php
				endforeach;
			@endphp
		</ul>
</div>