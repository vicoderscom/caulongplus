@php
    global $woocommerce, $product, $post;
    
@endphp
<div class="sidebar-4">
	<p class="title-sidebar">hàng mới về</p>

	@php
		$product_new = new WP_Query(array('post_type'=>'product','posts_per_page'=>7));

			foreach ($product_new->posts as $key => $value) :
				
				$id = $value->ID;

				$sanpham = get_field('san_pham_moi', $id);
                if(has_post_thumbnail($value->ID)) {
					$img = wp_get_attachment_url(get_post_meta( $id, '_thumbnail_id', true ));
                } else {
                	$img = get_template_directory_uri().'/resources/assets/images/no_image.svg';
                }
				$link_product_new = get_permalink($id);

				$title = get_the_title($id);

                $product = new WC_Product($id); 
                $regular_price = $product->regular_price;
                $price = $product->sale_price;

                $product_variale = new WC_Product_Variable( $value->ID);
	            $price_html = $product_variale->get_price_html();
	            if(empty($price_html)) {
                    $price_html = $product->get_price_html();
                    if($product->is_on_sale()) {
                        $is_on_sale = true;
                    } else {
                        $is_on_sale = false;
                    }
                    if($product->get_stock_status() === 'instock') {
                        $status_stock = 'instock';
                    } else {
                        $status_stock = 'outofstock';
                    }
                } 
				if($sanpham == "co"){
	@endphp


				<ul class="news-product">
					<li>
						<div class="img_product">
							<a href="{{$link_product_new}}"><img class="img-responsive" style="background-image: url({{ $img }});" src="{{ get_template_directory_uri() }}/resources/assets/images/sidebar/product_new.png"></a>
						</div>
						<div class="single_product">
							<p><a href="{{$link_product_new}}">@php echo $title; @endphp</a></p>
							<span class="price">{!! $price_html !!}</span>
						</div>
					</li>
				</ul>

	@php
				}
			endforeach;
	@endphp
</div>