<div class="page_cart">
	<div class="container">
	    <div class="col-md-12">
	    	<div class="row">
	    	    <div class="breadcrumb">
			        <div class="col-md-12">
			            <div class="row">
					        {!! woocommerce_breadcrumb() !!}
				        </div>
				    </div>
			    </div>
	    	    <div class="col-md-9 col-sm-12 col-xs-12 table_cart_form">
					{!! do_shortcode("[woocommerce_cart]") !!}
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 sidebar_cart">
					<div class="row">
						@php dynamic_sidebar('menu_sidebar_1'); @endphp

					    @php dynamic_sidebar('hotline_sidebar_2'); @endphp

					    @include( 'sidebar/sidebar_category')

					    @include( 'sidebar/sidebar_product_new')
					</div>
				</div>
	    	</div>
	    </div>
	</div>
</div>