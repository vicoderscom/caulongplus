<div class="page_product">
	<div class="container">
	    <div class="row">
		    <div class="col-md-12 breadcrum">
				{!! woocommerce_breadcrumb() !!}
			</div>
			<div class="col-md-12">
			    <div class="row">
					<div class="content content_home">
					    <div class="container">
					        @php
					            $product_cat = [
					                'taxonomy'=> 'product_cat',
					                'post_type' => 'product',
					                'parent' =>0,
					                'exclude' => 48
					            ];
					            $get_list_cates = get_categories($product_cat); 
					        @endphp
					        @foreach($get_list_cates as $val_cates)
					        @php
					            $cate_link = get_category_link($val_cates->term_id);
					        @endphp
					        <div class="col-md-12">
					            <div class="row">
					                <div class="list_product">
					                    <div class="cate_product">
					                        <h3 class="title_product active_title">
					                            <a href="{!! $cate_link !!}">{!! $val_cates->name !!}</a>
					                        </h3>
					                        @php
					                            $arr_cat_children = [
					                                'taxonomy'=> 'product_cat',
					                                'post_type' => 'product',
					                                'parent' =>$val_cates->term_id
					                            ];
					                            $cat_children = get_categories($arr_cat_children);
					                        @endphp
					                        @foreach($cat_children as $cat_children_val)
					                            @php
					                                $cate_link_children = get_category_link($cat_children_val->term_id);
					                            @endphp
					                            @if(!($arr_cat_children->term_id == 'san-pham-moi'))
					                            <h3 class="children">
					                                <a href="{!! $cate_link_children !!}">{!! $cat_children_val->name !!}</a> <span class="cat_bg_home_product"></span>
					                            </h3>
					                            @endif
					                        @endforeach
					                    </div>
					                    <ul class="list_item">
					                        @php
					                            $product_cat_1 = [
					                                'post_type'=>'product',
					                                'posts_per_page' => 8,
					                                'post_status'    => 'publish',
					                                'posts_per_page'=>10,
					                                'tax_query' => [
					                                    [
					                                        'taxonomy' => 'product_cat',
					                                        'field' => 'slug',
					                                        'terms' => $val_cates->slug,
					                                    ],
					                                ],
					                            ];
					                            $get_product = new WP_Query($product_cat_1);
					                            if($get_product->have_posts()) {
					                                foreach($get_product->posts as $key => $val) {
					                                    if(has_post_thumbnail($val->ID)) {
					                                        $img = wp_get_attachment_url(get_post_thumbnail_id($val->ID));
					                                    } else {
					                                        $img = get_template_directory_uri() . '/resources/assets/images/no_image.svg';
					                                    }
					                                    $link = get_permalink($val->ID);
					                                    
					                                    $product_variale = new WC_Product_Variable( $val->ID);
					                                    $sync_stock_status = $product_variale->sync_stock_status($val->ID);
					                                    $status_stock = $sync_stock_status->stock_status;
					                                    $is_on_sale = $product_variale->is_on_sale($val->ID);
					                                    $price_html = $product_variale->get_price_html();
					                                    
					                                    $product = new WC_Product($val->ID); 
					                                    $regular_price = $product->regular_price;
					                                    $price_sale = $product->sale_price;
					                                    $price = $product->price;

					                                    if(empty($price_html)) {
					                                        $price_html = $product->get_price_html();
					                                        if($product->is_on_sale()) {
					                                            $is_on_sale = true;
					                                        } else {
					                                            $is_on_sale = false;
					                                        }
					                                        if($product->get_stock_status() === 'instock') {
					                                            $status_stock = 'instock';
					                                        } else {
					                                            $status_stock = 'outofstock';
					                                        }
					                                    } 
					                                    
					                                    @endphp
					                                        <li class="col-md-3 col-sm-4 col-xs-6">
					                                            <div class="status_stock">
					                                                @if($status_stock === 'instock')
					                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/instock.png" alt="">
					                                                @else
					                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/outstock.png" alt="">
					                                                @endif
					                                            </div>
					                                            <div class="img">
					                                                <a href="{{ $link }}">
					                                                    <img style="background-image: url({{ $img }});" src="{{ get_template_directory_uri() }}/resources/assets/images/product.png" alt="">
					                                                </a>
					                                            </div>
					                                            @if($is_on_sale)
					                                            <div class="mess_sale">
					                                                <img src="{{ get_template_directory_uri() }}/resources/assets/images/home/mess_sale.png" alt="">
					                                            </div>
					                                            @endif
					                                            <div class="desc">
					                                                <h3>
					                                                    <a href="{{ $link }}">{!! $val->post_title !!}</a>
					                                                </h3>
					                                                <span class="price">{!! $price_html !!}</span>
					                                            </div>
					                                        </li>
					                                    @php
					                                }
					                            }
					                        @endphp
					                    </ul>
					                </div>
					            </div>
					        </div>
					        @endforeach
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@php
	
@endphp