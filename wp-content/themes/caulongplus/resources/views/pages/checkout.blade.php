<div class="page_checkout">
	<div class="container">
	    <div class="col-md-12">
	    	<div class="row">
	    	    <div class="breadcrumb">
			        <div class="col-md-12">
			            <div class="row">
					        {!! woocommerce_breadcrumb() !!}
				        </div>
				    </div>
			    </div>
	    	    <div class="col-md-9 col-sm-9 col-xs-12">
					{!! do_shortcode("[woocommerce_checkout]") !!}
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="row">
						@php dynamic_sidebar('menu_sidebar_1'); @endphp

					    @php dynamic_sidebar('hotline_sidebar_2'); @endphp

					    @include( 'sidebar/sidebar_product_new')
					</div>
				</div>
	    	</div>
	    </div>
	</div>
</div>