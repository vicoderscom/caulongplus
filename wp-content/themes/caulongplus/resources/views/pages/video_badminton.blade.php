<div class="container page_video">
	<div class="row">
		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>
		
		<div class="col-md-9 col-sm-7 list-item">

			<div class="title_list_video">
				<p>Video</p>
			</div>

			<ul class="row list_video description">

				@php
					$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

					$list_video = array(
						'post_type'      => 'video',
						'posts_per_page' => 12,
						'paged' => $paged,
					);

					$video = new WP_Query($list_video);
					$total_pages = $video->max_num_pages;

					if($video->have_posts()):
						while ( $video->have_posts() ) : $video->the_post();

						$img_video = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
		                $link_video = get_permalink($post->ID);
				@endphp

						<li class="item_video col-md-3">
							<div class="img_video">
								<a href="{{ $link_video }}"><img class="image" style="background-image: url({{ $img_video }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/video/video.png" alt=""></a>

								<a href="{{ $link_video }}"><img class="icon_video" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/video/youtube-icon.png" alt=""></a>

							</div>
							<p class="title_post_video"><a href="{{ $link_video }}">@php the_title(); @endphp</a></p>
						</li>

				@php
						endwhile;
					endif;
				@endphp

			</ul>
				@php
					// Loop query to get data and display on monitor
					echo "<div class='paginate pull-right'>";
					if ($total_pages > 1){
					    $current_page = max(1, $paged);
					    echo paginate_links(array(
					        'base' => @add_query_arg('trang','%#%'),
					        'format' => '?trang=%#%',
					        'current' => $current_page,
					        'total' => $total_pages,
					    ));
					                            
					}
					echo "</div>";
					wp_reset_postdata();
				@endphp
		</div>

		<div class="col-md-3 col-sm-5 sidebar-page">
			@php
				dynamic_sidebar('menu_sidebar_1');
			@endphp

			@php
				dynamic_sidebar('hotline_sidebar_2');
			@endphp
			

			@include( 'sidebar/sidebar_category')


			@include( 'sidebar/sidebar_product_new')
		</div>
	</div>
</div>


