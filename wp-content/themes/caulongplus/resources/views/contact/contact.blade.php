<div class="container page_contact">
	<div class="row">

		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>
		
		<div class="col-md-9 col-sm-7 col-xs-12 list-item">
			@php
				dynamic_sidebar('contact_page');
			@endphp
		</div>

		<div class="col-md-3 col-sm-5 col-xs-12 sidebar-page">
			@php
				dynamic_sidebar('menu_sidebar_1');
			@endphp
			
			@php
				dynamic_sidebar('hotline_sidebar_2');
			@endphp		

			@include( 'sidebar/sidebar_category')


			@include( 'sidebar/sidebar_product_new')
		</div>
	</div>
</div>