@php
	$object_current = get_queried_object();
@endphp
<div class="page_product">
	<div class="container">
	    <div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>
		<div class="col-md-12">
			<div class="content content_home">
		        <div class="col-md-12">
		            <div class="row">
		                <div class="list_product">
		                    <div class="col-md-12">
		                    	<div class="row">
				                    <div class="cate_product">
				                        <h3 class="title_product">{!! $object_current->name !!}</h3>
				                        <div class="sort_product pull-right" id="sort_product">
				                        	<select name="" id="">
				                        		<option value="">Sắp xếp theo</option>
				                        		<option value="max_to_min">Giá từ cao đến thấp</option>
				                        		<option value="min_to_max">Giá từ thấp đến cao</option>
				                        	</select>
				                        </div>
				                    </div>
		                    	</div>
		                    </div>
		                    <div class="content_cat_product">
			                    <div class="col-md-12">
			                    	<div class="row">
					                    <ul>
					                        @php
					                            $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
					                            $product_cat_1 = [
					                                'post_type'=>'product',
					                                'posts_per_page' => 30,
					                                'post_status'    => 'publish',
					                                'tax_query' => [
				                                        [
				                                           'taxonomy' =>'product_cat',
				                                           'field' => 'slug',
				                                           'terms' =>$object_current->slug
				                                        ]
				                                    ],
				                                    'paged' => $paged,
					                            ];
					                            $get_product = new WP_Query($product_cat_1);
					                            if($get_product->have_posts()) : 
					                                foreach($get_product->posts as $key => $val) {
						                                if(has_post_thumbnail($val->ID)) {
					                                        $img = wp_get_attachment_url(get_post_thumbnail_id($val->ID));
					                                    } else {
					                                        $img = get_template_directory_uri() . '/resources/assets/images/no_image.svg';
					                                    }
					                                    $link = get_permalink($val->ID);
					                                    
					                                    $product_variale = new WC_Product_Variable( $val->ID);
					                                    $sync_stock_status = $product_variale->sync_stock_status($val->ID);
					                                    $status_stock = $sync_stock_status->stock_status;
					                                    $is_on_sale = $product_variale->is_on_sale($val->ID);
					                                    $price_html = $product_variale->get_price_html();
					                                    
					                                    $product = new WC_Product($val->ID); 
					                                    $regular_price = $product->regular_price;
					                                    $price_sale = $product->sale_price;
					                                    $price = $product->price;

					                                    if(empty($price_html)) {
					                                        $price_html = $product->get_price_html();
					                                        if($product->is_on_sale()) {
					                                            $is_on_sale = true;
					                                        } else {
					                                            $is_on_sale = false;
					                                        }
					                                        if($product->get_stock_status() === 'instock') {
					                                            $status_stock = 'instock';
					                                        } else {
					                                            $status_stock = 'outofstock';
					                                        }
					                                    } 
						                                @endphp
						                                    <li class="col-md-3 col-sm-4 col-xs-6">
					                                            <div class="status_stock">
					                                                @if($status_stock === 'instock')
					                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/instock.png" alt="">
					                                                @else
					                                                    <img src="{{ get_template_directory_uri() }}/resources/assets/images/products/outstock.png" alt="">
					                                                @endif
					                                            </div>
					                                            <div class="img">
					                                                <a href="{{ $link }}">
					                                                    <img style="background-image: url({{ $img }});" src="{{ get_template_directory_uri() }}/resources/assets/images/product.png" alt="">
					                                                </a>
					                                            </div>
					                                            @if($is_on_sale)
					                                            <div class="mess_sale">
					                                                <img src="{{ get_template_directory_uri() }}/resources/assets/images/home/mess_sale.png" alt="">
					                                            </div>
					                                            @endif
					                                            <div class="desc">
					                                                <h3>
					                                                    <a href="{{ $link }}">{!! $val->post_title !!}</a>
					                                                </h3>
					                                                <span class="price">{!! $price_html !!}</span>
					                                            </div>
					                                        </li>
						                                @php
					                                }
					                            endif;
					                        @endphp
					                    </ul>
			                    	</div>
			                    </div>
			                    <div class="col-md-12">
			                    	<div class="row">
					                    <div class="wrap_paginate">
						                    <div class="paginate pull-right">
								                <?php
								                $total_pages = $get_product->max_num_pages;

								                if ($total_pages > 1) :

								                    $current_page = max(1, $paged);

								                    echo paginate_links(array(
												        'base' => @add_query_arg('trang','%#%'),
												        'format' => '?trang=%#%',
												        'current' => $current_page,
												        'total' => $total_pages,
								                        'prev_text'    => __('<<'),
								                        'next_text'    => __('>>')
												    ));
								                ?>    
								                <?php endif; ?>
								                <?php wp_reset_postdata(); ?>
					                        </div>
							            </div>
			                    	</div>
			                    </div>
			                </div>
		                </div>
		            </div>
		        </div>
			</div>
		</div>
	</div>
</div>
<div id="loader_page">
	<img src="{{ get_template_directory_uri() }}/resources/assets/images/loader.gif" alt="">
</div>
@include('home.sort_product.sort_product')