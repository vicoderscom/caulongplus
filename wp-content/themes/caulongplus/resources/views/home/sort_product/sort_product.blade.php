@php
    $url_site = get_site_url();
    $object_current_slug = $object_current->slug;
    $current_domain = get_option('home');
    $current_url = $current_domain .'/danh-muc/'. $object_current->slug;
@endphp
<script>
jQuery(function($){
    function sort_product() {
        var url_site = "<?php echo $url_site; ?>";
        var cat_current_slug = "<?php echo $object_current_slug; ?>";
        var current_url = "<?php echo $current_url; ?>";

        $("#sort_product select").change(function(){
            $('#loader_page').show();
            var val_option = $(this).val();
            var price_meta = '';
            var order_sort = '';
            if(val_option == 'min_to_max') {
                price_meta = '_price';
                order_sort = 'asc';
            } else if(val_option == 'max_to_min') {
                price_meta = '_price';
                order_sort = 'desc';
            } else {}
            var ppp = 12;
            var pageNumber = 1;
            var offset = 0;
            var data_url = url_site + '/wp-admin/admin-ajax.php';
            var str = 'ppp=' + ppp + '&pageNumber=' + pageNumber + '&action=my_action'+'&offset='+offset + '&cat_current_slug=' + cat_current_slug + '&price_meta=' + price_meta + '&order_sort=' + order_sort + '&current_url=' + current_url;
            try {
                $.ajax({
                    type: 'POST',
                    url: data_url,
                    data: str,
                    success: function(response) {
                        if(response) {
                            $('#loader_page').hide();
                        }
                        $('.content_cat_product').empty();
                        $('.content_cat_product').html(response);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            } catch(e) {
                alert('Bạn quên một cái gì đó. Vui lòng xem lại ');
            }
        });
    }
    sort_product();
    
});
</script>