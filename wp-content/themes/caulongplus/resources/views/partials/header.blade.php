<!DOCTYPE html>
<html style="margin-top:0 !important">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
    @if(is_home())
        {!! 'Trang chủ' !!}
    @endif
    @php
        wp_title(''); 
        echo " - " . get_bloginfo('description');
    @endphp
    </title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon.jpg" />

    @php wp_head(); @endphp
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="{{ get_template_directory_uri() }}/resources/assets/js/elevatezoom-master/jquery.elevatezoom.js"></script>
</head>

<body>
    <div id="fb-root"></div>
    <div id="fb-root"></div>
    <div class="wraper home">
        <div class="header">
            <div class="header_site">
                <div class="col-md-12" style="background-image: url({{ get_template_directory_uri() }}/resources/assets/images/home/header_top.png);">
                    <div class="row">
                        <div class="header_top container">
                            <div class="col-md-3 col-sm-3">
                                <div class="row">
                                    <div class="logo">
                                        @php
                                            $customLogoId = get_theme_mod('custom_logo');
                                            $logo = wp_get_attachment_image_src($customLogoId , 'full');
                                        @endphp
                                        <a href="/">
                                            <img src="{{ $logo[0] }}" />

                                            <h3>{!! get_option('blogname') !!}</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-6">
                                <div class="row">
                                    {{-- {!! get_search_form() !!} --}}
                                    {{-- {!! do_shortcode('[wpdreams_ajaxsearchlite]') !!} --}}
                                    <?php echo do_shortcode("[wpns_search_form only_search='product']"); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 header_cart">
                                <div class="row">
                                    <div class="cart">
                                        <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
                                            <img src="{{ get_template_directory_uri() }}/resources/assets/images/home/cart.png" alt="">
                                            <span class="quantity">
                                                {!! sprintf ( _n( '%d', '%d ', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ) !!}
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="header_bottom">
                            <nav class="navbar navbar-default navbar-static-top" 
                                style="background-image: url({{ get_template_directory_uri() }}/resources/assets/images/home/menu.png);"
                            >
                                <div class="container">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="collapse navbar-collapse content_menu_header" id="bs-example-navbar-collapse-1">
                                        <div class="contai_menu">
                                            @php 
                                                wp_nav_menu(
                                                    [ 
                                                        'menu' => 'Menu', 
                                                        'theme_location' => 'top', 
                                                        'menu_id' => 'top-menu', 
                                                        'menu_class' => 'nav-menu nav navbar-nav', 
                                                        'container' => ''
                                                    ]
                                                ); 
                                            @endphp
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
