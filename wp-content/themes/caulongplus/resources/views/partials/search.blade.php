<div class="page_search">
	<div class="container">
	    <div class="col-md-12">
	    	<div class="row">
	    	    <div class="breadcrumb">
			        <div class="col-md-12">
			            <div class="row">
					        {!! woocommerce_breadcrumb() !!}
				        </div>
				    </div>
			    </div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="content_search">
						<div class="col-md-12">
							<div class="content content_home">
						        <div class="col-md-12">
						            <div class="row">
						                <div class="list_product">
						                    <div class="cate_product">
						                        <h3 class="title_product">@php the_title(); @endphp</h3>
						                    </div>
						                    <div class="col-md-12">
						                    	<div class="row">
								                    <ul>
								                        @php
								                        if(isset($_GET['s'])) {
										                    $keySearch = esc_attr($_GET['s']);
										                    if(empty($keySearch)) {
										                        echo "Không tìm thấy kết quả với từ khoá: " . $keySearch;
										                    } else {
										                        $query_string = get_search_query();
										                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
										                        $args = array(                      
									                                'post_type' => array('product'),                                                                                             
									                                'meta_query' => array(
									                                    'relation' => 'OR',
									                                    array(
								                                            'key' => 'tab_retail',
									                                        'value' => $query_string,
									                                        'compare' => 'LIKE'
									                                    ),
									                                    array(
									                                        'key' => 'tab_tech',
									                                        'value' => $query_string,
									                                        'compare' => 'LIKE'
									                                    ),
									                                    array(
									                                        'key' => 'tab_shipping_information',
									                                        'value' => $query_string,
									                                        'compare' => 'LIKE'
									                                    )
									                                ),                      
									                                'posts_per_page' => 30,
									                                'paged' => $paged                     
										                        );
										                        function filter_where_title($where = '') {
										                            $query_string = get_search_query();
										                            $where .= " OR post_title LIKE '%".$query_string."%' OR post_content LIKE '%".$query_string."%'";                   
										                            return $where;
										                        }
										                        $posts = new WP_Query();
										                        add_filter('posts_where', 'filter_where_title');
										                        $search_posts = $posts->query($args);  
										                        if(!empty($search_posts)) {
										                            foreach($search_posts as $search_post) {
										                                $img = wp_get_attachment_url(get_post_thumbnail_id($search_post->ID));
										                                $link = get_permalink($search_post->ID);
										                            	// var_dump($image_link);
										                                @endphp
										                                    <li class="col-md-3 col-sm-4 col-xs-6">
										                                        @if($price_sale > 0)
										                                        <div class="state" style="background-image: url({{ get_template_directory_uri() }}/resources/assets/images/home/sale_percent.png)">
										                                            {!! $scale_price .'%' !!}
										                                        </div>
										                                        @endif
										                                        <div class="img">
										                                            <a href="{{ $link }}">
										                                                <img style="background-image: url({{ $img }});" src="{{ get_template_directory_uri() }}/resources/assets/images/product.png" alt="">
										                                            </a>
										                                        </div>
										                                        @if($price_sale > 0)
										                                        <div class="mess_sale">
										                                            <img src="{{ get_template_directory_uri() }}/resources/assets/images/home/mess_sale.png" alt="">
										                                        </div>
										                                        @endif
										                                        <div class="desc">
										                                            <h3>
										                                                <a href="{{ $link }}">{!! $search_post->post_title !!}</a>
										                                            </h3>
										                                            <span class="price">{!! wc_price($regular_price) !!}</span>
										                                        </div>
										                                    </li>
										                                <?php

										                            remove_filter('posts_where', 'filter_where_title');
										                            }
										                        } else {
										                            echo "Không có kết quả nào tìm kiếm bằng từ khóa trên";
										                        }
										                    }
										                }
										                ?>
								                    </ul>
						                    	</div>
						                    </div>
						                    <div class="col-md-12">
						                    	<div class="row">
								                    <div class="wrap_paginate">
									                    <div class="paginate pull-right">
											                <?php
											                $total_pages = $get_product->max_num_pages;

											                if ($total_pages > 1) :

											                    $current_page = max(1, get_query_var('paged'));

											                    echo paginate_links(array(
											                        'base' => get_pagenum_link(1) . '%_%',
											                        'format' => 'page/%#%',
											                        'current' => $current_page,
											                        'total' => $total_pages,
											                        'prev_text'    => __('<<'),
											                        'next_text'    => __('>>'),
											                    ));
											                ?>    
											                <?php endif; ?>
											                <?php wp_reset_postdata(); ?>
								                        </div>
										            </div>
						                    	</div>
						                    </div>
						                </div>
						            </div>
						        </div>
							</div>
						</div>
					</div>
				</div>
	    	</div>
	    </div>
	</div>
</div>