	<div class="footer_link">
		<div class="footer_top">
			<div class="container">
				<div class="col-md-3 col-sm-3 col-xs-12">
				    <h3>Thông tin siêu thị cầu lông</h3>
				    @php
				    	wp_nav_menu(
	                        [
	                            'menu' => 'Footer information',
	                            'theme_location' => 'top',
	                            'menu_id' => 'top-menu',
	                            'menu_class' => '',
	                            'container' => ''
	                        ]
	                    );
			    	@endphp
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
				    <h3>Chính sách công ty</h3>
					@php
				    	wp_nav_menu(
	                        [
	                            'menu' => 'Footer policy',
	                            'theme_location' => 'top',
	                            'menu_id' => 'top-menu',
	                            'menu_class' => '',
	                            'container' => ''
	                        ]
	                    );
			    	@endphp
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
				    <h3>Hỗ trợ khách hàng</h3>
					@php
				    	wp_nav_menu(
	                        [
	                            'menu' => 'Footer support',
	                            'theme_location' => 'top',
	                            'menu_id' => 'top-menu',
	                            'menu_class' => '',
	                            'container' => ''
	                        ]
	                    );
			    	@endphp
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
				    <h3>Tin tức liên hệ</h3>
					@php
				    	wp_nav_menu(
	                        [
	                            'menu' => 'Footer news',
	                            'theme_location' => 'top',
	                            'menu_id' => 'top-menu',
	                            'menu_class' => '',
	                            'container' => ''
	                        ]
	                    );
			    	@endphp
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="footer_top">
			<div class="container">

			</div>
		</div>
		<div class="footer_bottom">
			<div class="container">
				<div class="col-md-7 col-sm-6">
					<div class="row">
						<div class="col-md-5 menu_footer">
							@php
								dynamic_sidebar('menu_footer_one');
							@endphp
						</div>
						<div class="col-md-7">
							@php
							if(is_active_sidebar('footer_fanpage_facebook')) :
	                           dynamic_sidebar('footer_fanpage_facebook');
	                        endif
						@endphp
						</div>
					</div>
				</div>
				<div class="col-md-5 col-sm-6 footer_text">
					<div class="row">
						@php
							if(is_active_sidebar('footer_contact')) :
	                           dynamic_sidebar('footer_contact');
	                        endif
						@endphp
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@php
	wp_footer();
@endphp
<div class="fb-chatdialog" style="position:relative">
		<button class="btn btn-primary btn-fb-dialog" style="position: fixed; bottom: 0; right: 0px; color: #fff;">Gửi tin nhắn cho chúng tôi</button>
		<iframe class="fb-iframe" src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/Sieuthicaulong.vn/&tabs=messages&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1781200335525186
" width="340" height="500" data-show="none" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.btn-fb-dialog').css({'display' : 'block', 'z-index' : '1000'});
			$('.fb-iframe').css({'display' : 'none'});
			var height_ifr = $('.fb-iframe').height();
			console.log(height_ifr);
			$('.btn-fb-dialog').click(function(){
				var show = $('.fb-iframe').attr('data-show');
				if(show == 'none') {
					$('.fb-iframe').css({'display' : 'block','position': 'fixed', 'z-index' : '1000', 'bottom' : '0', 'right' : '0'});
					$('.btn-fb-dialog').css({'bottom' : height_ifr});
					$('.fb-iframe').attr('data-show', 'show');
					$('.btn-fb-dialog').text('Đóng x');
				} else {
					$('.fb-iframe').attr('data-show', 'none');
					$('.fb-iframe').css({'display' : 'none', 'bottom' : '0'});
					$('.btn-fb-dialog').css({'bottom' : '0'});
					$('.btn-fb-dialog').text('Gửi tin nhắn cho chúng tôi ');
				}
			});
		});
	</script>
</body>

</html>
