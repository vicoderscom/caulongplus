<div class="container news-page">
	
	<div class="row">

		<div class="col-md-12 breadcrum">
			{!! woocommerce_breadcrumb() !!}
		</div>



		<div class="col-md-9 col-sm-7 list-item">
			<ul class="item">
				@php
					$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
				    $object_page = get_queried_object();
				    $object_slug = $object_page->slug;

					$list_new = array(
						'post_type' => 'news',
						'post_status' => 'publish',
						'tax_query' => [
		                                   [
		                                      'taxonomy' =>'category_news',
		                                      'field' => 'slug',
		                                      'terms' =>$object_slug
		                                   ]
		                               ],
						'posts_per_page' => 8,
						'paged' => $paged
					);

					$news = new WP_Query($list_new);

					if ($news->have_posts()) :
						while ( $news->have_posts() ) : $news->the_post();
					        if(has_post_thumbnail()){
								$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
					        }
						    else {
						    	$img = get_stylesheet_directory_uri().'/resources/assets/images/no-image-available.jpg';
						    }
		                    $link = get_permalink($post->ID);
							@endphp
							<li class="li-item row">
				  				<div class="col-md-4 col-sm-12 image-item">
									<a class="url_news" href="{{ $link }}"><img class="img-responsive" style="background-image: url({{ $img }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/news/demo.png" alt="anh mau"></a>
								</div>

								<div class="col-md-8 col-sm-12 content-news">
									<h3 class="title-post">
										<a href="{{ $link }}">
											@php the_title(); @endphp
										</a>
									</h3>
									<p class="post_content">@php echo get_the_popular_excerpt(300); @endphp</p>
									<span><a href="{{ $link }}">xem tiếp <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
								</div>
							</li>
							
							@php
						endwhile; wp_reset_query();
					endif;
					// Loop query to get data and display on monitor
					@endphp
					<div class="wrap_paginate">
						<?php
							$total_pages = $news->max_num_pages;
							echo "<div class='paginate pull-right'>";
							if ($total_pages > 1){
							    $current_page = max(1, $paged);
							    echo paginate_links(array(
							        'base' => @add_query_arg('trang','%#%'),
							        'format' => '?trang=%#%',
							        'current' => $current_page,
							        'total' => $total_pages,
							    ));
							                            
							}
							echo "</div>";
							wp_reset_postdata();
					    ?>
					</div>
			</ul>
		</div>

		<div class="col-md-3 col-sm-5 sidebar-page">
			
			@php
				dynamic_sidebar('menu_sidebar_1');
			@endphp

			@php
				dynamic_sidebar('hotline_sidebar_2');
			@endphp

			@include( 'sidebar/sidebar_category')


			@include( 'sidebar/sidebar_product_new')

		</div>
	</div>
</div>