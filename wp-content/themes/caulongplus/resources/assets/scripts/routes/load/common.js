(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=731670450343341";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// (function(d, s, id) {
//     var js, fjs = d.getElementsByTagName(s)[0];
//     if (d.getElementById(id)) return;
//     js = d.createElement(s);
//     js.id = id;
//     js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=513845042135270";
//     fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));

(function($) {
    $.fn.openSelect = function()
    {
        return this.each(function(idx,domEl) {
            if (document.createEvent) {
                var event = document.createEvent("MouseEvents");
                event.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                domEl.dispatchEvent(event);
            } else if (element.fireEvent) {
                domEl.fireEvent("onmousedown");
            }
        });
    }
}(jQuery));


jQuery(function($) {
    $('.gallery_list').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 3000000,
    });

    $('html').attr('style', 'margin-top: 0 !important');
    $('.sidebar-3 ul li').click(function() {
        $(this).children('ul').toggle();
    });


    // slider woocommerce
    $('.woocommerce-product-gallery ').css({
        opacity: 1
    });
    $('.carousel-inner .item:first').addClass('active');

    var number_plus = $('.quantity .fa-plus-square');
    var number_minus = $('.quantity .fa-minus-square');
    var val_input_quantity = $('.quantity input').val();

    // custom page cart
    // var plus_square $('.fa-plus-square')
    // end custom page cart

    $('.plus').on('click', function(e) {

        var val = parseInt($(this).prev('input').val());

        $(this).prev('input').val(val + 1);

        $('.update-cart').removeAttr('disabled');
        $('input[name="update_cart"]').removeAttr('disabled');

    });

    $('.minus').on('click', function(e) {

        var val = parseInt($(this).next('input').val());

        if (val !== 0) {
            $(this).next('input').val(val - 1);
        }

        $('.update-cart').removeAttr('disabled');
        $('input[name="update_cart"]').removeAttr('disabled');

    });

    $('.product-column').mousemove(function(e) {

        var parentOffset = $(this).offset();
        var relX = e.pageX - parentOffset.left + 10;
        var relY = e.pageY - parentOffset.top;

        $(this).children('.product-item-hidden').css({
            'top': relY + 'px',
            'left': relX + 'px'
        }).show();
    }).mouseout(function() {
        $(this).children('.product-item-hidden').hide();
    });

    $('.btn-buy').click(function(e) {
        e.preventDefault();

        var productId = $(this).attr('data-id');

        var data = {
            action: 'ajax_add_to_cart_single',
            productId: productId,
        };

        $.post(ajax_obj.ajaxurl, data, function(resp) {
            var response = $.parseJSON(resp);

            if (response.status === 0) {
                window.location = response.redirect;
            }
        });
    });


});


jQuery(function($) {

    // start menu 
    function menu_res() {
        var window_site = window.innerWidth;
        var top_menu = $('#top-menu');
        var content_menu_header = $('.content_menu_header');
        var menu_site_chilren = $('.content_menu_header .contai_menu');
        var search_site = $('.search_site');
        var header_top = $('.header_top');
        if (window_site > 768) {
            var content_menu_header_width = content_menu_header.width();
            var top_menu_width = top_menu.width();
            var menu_site_chilren_width = $('.content_menu_header .contai_menu').width();
            if ($('.header .navbar-collapse ul').hasClass('cart_menu')) {
                top_menu.css({
                    opacity: 1,
                });
                menu_site_chilren.css({
                    position: 'absolute',
                    left: '50%',
                    marginLeft: -menu_site_chilren_width / 2,
                });
            } else {
                top_menu.css({
                    left: '50%',
                    position: 'absolute',
                    marginLeft: -(top_menu_width) / 2,
                    opacity: 1
                });
                menu_site_chilren.css({
                    display: 'block'
                })
            }

            var search_site_width = $('.search_site').width();
            var header_top_width = $('.header_top').width();
            search_site.css({
                opacity: 1,
                left: '50%',
                marginLeft: -(search_site_width) / 2,
            });
        } else {
            if ($('.header .navbar-collapse ul').hasClass('cart_menu')) {
                top_menu.css({
                    opacity: 1,
                });
                menu_site_chilren.css({
                    position: 'static',
                    left: '0',
                    marginLeft: '0',
                    display: 'block'
                });
            } else {
                top_menu.css({
                    left: '0',
                    position: 'static',
                    marginLeft: '-15px',
                    opacity: 1
                });
                menu_site_chilren.css({
                    display: 'block'
                })
            }
            search_site.css({
                opacity: 1,
                left: '0',
                marginLeft: '0',
            });
        }
    }
    menu_res();
    $(window).resize(function() {
        menu_res();
    });
    // end menu
    $('.footer_link ul li a').before('<i class="fa fa-chevron-right" aria-hidden="true"></i>');

    var img_zoom = $('.image_product_view .image_default img');
    var img_zoom_list = $('.image_product_view .gallery_list img');
    // var img_zoom_get_atrr = img_zoom.attr('src');
    // img_zoom.attr("data-zoom-image", img_zoom_get_atrr);
    // img_zoom.addClass('room_img_product');
    // function set_zoom() {
    //     $(".room_img_product").elevateZoom({scrollZoom : true});
    // }
    // set_zoom();
    img_zoom.click(function(){
        return false;
    });
    img_zoom_list.click(function(){
        var get_img_src = $(this).attr('style'); console.log(get_img_src);
        var get_img_src_style = $(this).css('background-image'); 
        get_img_src_style = get_img_src_style.replace('url(','').replace(')','').replace(/\"/gi, "");
        img_zoom.attr('data-zoom-image', get_img_src_style);  
        img_zoom.attr('style', get_img_src); 
        return false;
    });

    $('.fa-caret-down').click(function(){
        $('.product-cates').click();
    });
    
    function breadcrumb_taxonomy() {
        var breadcrumb_html = $('.woocommerce-breadcrumb').html();
        var res = (!isNaN(breadcrumb_html)) ? breadcrumb_html.replace('/&nbsp;category_news'," ") : breadcrumb_html;
        // var res = breadcrumb_html.replace('/&nbsp;category_news', '');
        $('.woocommerce-breadcrumb').html(res);
    }

    function breadcrumb_post_type() {
        var breadcrumb_html = $('.woocommerce-breadcrumb').html();
        var res_pos = (!isNaN(breadcrumb_html)) ? breadcrumb_html.replace('news</a>&nbsp;/&nbsp;'," ") : breadcrumb_html;
        // var res_pos = breadcrumb_html.replace('news</a>&nbsp;/&nbsp;', '');
        $('.woocommerce-breadcrumb').html(res_pos);
    }
    function title_destroy() {
        var title_destroy = $('head > title').html();
        var res_pos = title_destroy.replace('category_news', '');
        $('head > title').html(res_pos);
    }

    breadcrumb_taxonomy();
    breadcrumb_post_type();
    title_destroy();
});
