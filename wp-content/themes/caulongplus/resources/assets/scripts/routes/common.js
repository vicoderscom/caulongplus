// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';

export default {
  init() {
    require('./load/common.js');
    require('./load/options.js');
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
