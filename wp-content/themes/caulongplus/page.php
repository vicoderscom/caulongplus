<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.pages');

get_footer();
?>
