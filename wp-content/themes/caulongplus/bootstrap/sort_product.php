<?php
function data_ajax() { 
	$ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 12;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
    $offset = (isset($_POST['offset'])) ? $_POST['offset'] : 0;
    $cat_current_slug = (isset($_POST['cat_current_slug'])) ? $_POST['cat_current_slug'] : 'desc';
    $price_meta = (isset($_POST['price_meta'])) ? $_POST['price_meta'] : '';
    $order_sort = (isset($_POST['order_sort'])) ? $_POST['order_sort'] : '';
    $current_url = (isset($_POST['current_url'])) ? $_POST['current_url'] : '';
	?>
	<div class="col-md-12">
		<div class="row">
	        <ul>
			<?php
		    $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
		    $product_cat_1 = [
		        'post_type'=>'product',
		        'posts_per_page' => 30,
		        'post_status'    => 'publish',
		        'orderby' => 'meta_value_num',
				'meta_key' => $price_meta,
				'order' => $order_sort,
		        'tax_query' => [
		            [
		               'taxonomy' =>'product_cat',
		               'field' => 'slug',
		               'terms' =>$cat_current_slug
		            ]
		        ],
		        'paged' => $paged,
		    ];
		    $get_product = new WP_Query($product_cat_1);
		    if($get_product->have_posts()) {
		        foreach($get_product->posts as $key => $val) {
		            if(has_post_thumbnail($val->ID)) {
		                $img = wp_get_attachment_url(get_post_thumbnail_id($val->ID));
		            } else {
		                $img = get_template_directory_uri() . '/resources/assets/images/no_image.svg';
		            }
		            $link = get_permalink($val->ID);
		            
		            $product_variale = new WC_Product_Variable( $val->ID);
		            $sync_stock_status = $product_variale->sync_stock_status($val->ID);
		            $status_stock = $sync_stock_status->stock_status;
		            $is_on_sale = $product_variale->is_on_sale($val->ID);
		            $price_html = $product_variale->get_price_html();
		            
		            $product = new WC_Product($val->ID); 
		            $regular_price = $product->regular_price;
		            $price_sale = $product->sale_price;
		            $price = $product->price;

		            if(empty($price_html)) {
		                $price_html = $product->get_price_html();
		                if($product->is_on_sale()) {
		                    $is_on_sale = true;
		                } else {
		                    $is_on_sale = false;
		                }
		                if($product->get_stock_status() === 'instock') {
		                    $status_stock = 'instock';
		                } else {
		                    $status_stock = 'outofstock';
		                }
		            } 
		            ?>
		                <li class="col-md-3 col-sm-4 col-xs-6">
		                    <div class="status_stock">
		                        <?php if($status_stock === 'instock') { ?>
		                            <img src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/products/instock.png" alt="">
		                        <?php } else { ?>
		                            <img src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/products/outstock.png" alt="">
		                        <?php } ?>
		                    </div>
		                    <div class="img">
		                        <a href="<?php echo $link; ?>">
		                            <img style="background-image: url(<?php echo $img; ?>);" src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/product.png" alt="">
		                        </a>
		                    </div>
		                    <?php if($is_on_sale) { ?>
		                    <div class="mess_sale">
		                        <img src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/home/mess_sale.png" alt="">
		                    </div>
		                    <?php } ?>
		                    <div class="desc">
		                        <h3>
		                            <a href="<?php echo $link; ?>"><?php echo $val->post_title; ?></a>
		                        </h3>
		                        <span class="price"><?php echo $price_html; ?></span>
		                    </div>
		                </li>
		            <?php
		        }
		    } ?>
	        </ul>
        </div>
    </div>
    <div class="col-md-12">
    	<div class="row">
            <div class="wrap_paginate">
                <div class="paginate pull-right">
	                <?php
	                $total_pages = $get_product->max_num_pages;

	                if ($total_pages > 1) :

	                    $current_page = max(1, $paged);

	                    echo paginate_links(array(
					        'base' => $current_url. '%_%',
					        'format' => '?trang=%#%',
					        'current' => $current_page,
					        'total' => $total_pages,
	                        'prev_text'    => __('<<'),
	                        'next_text'    => __('>>')
					    ));
	                ?>    
	                <?php endif; ?>
	                <?php wp_reset_postdata(); ?>
                </div>
            </div>
    	</div>
    </div> 
   <?php
    wp_die();
} 

