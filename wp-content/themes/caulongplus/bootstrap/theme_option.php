<div class="theme_option_site">
    <h3 class="title_site">* Quản lý theme options</h3>
    <form method="post" action="">
        <table>
            <tr>
                <td>Font family </td>
                <td class="input_td">
                    <select name="font_family" id="">
                        <?php
                            $colect_fonts = ['tahoma' => 'Tahoma', 'times_new_roman' => 'Times New Roman'];
                            foreach($colect_fonts as $key => $val) {
                        ?>
                        <option <?php if($font_family == $key) {echo "selected";} ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        </table>
        <input type="submit" name="save_theme_option" value="Save Theme Option" class="input_theme_option btn btn-primary" />
    </form>
</div>
