<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');
/**
 * Theme support
 */
function theme_enqueue_style() {
	wp_enqueue_style(
		'template-style',
		get_stylesheet_directory_uri() . '/dist/styles/main.css',
		false
	);
}

function theme_enqueue_scripts() {
	wp_enqueue_script(
		'template-scripts-js',
		get_stylesheet_directory_uri() . '/bower_components/jquery/dist/jquery.min.js',
		['jquery-js']
	);

	wp_enqueue_script(
		'template-scripts',
		get_stylesheet_directory_uri() . '/dist/scripts/main.js',
		['jquery']
	);
}
