<?php /* Template Name: Page video badminton */ ?>
<?php
use NF\View\Facades\View;

get_header();

echo View::render('pages.video_badminton');

get_footer();
