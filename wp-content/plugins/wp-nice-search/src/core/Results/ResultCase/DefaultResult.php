<?php
namespace core\Results\ResultCase;

use core\Results\Results as Results;

/**
 * This class create a default list with title and icons
 * @package wpns
 * @since 1.0.6
 */
class DefaultResult extends Results
{
	/**
	 * Initiliaze
	 * @since 1.0.7
	 */
	public function __construct($s = '', $cate = '')
	{
		$this->keyword = $s;
		$this->cate = $cate;
		parent::__construct();
	}

	/**
	 * create a list results with featured image
	 * @return string $lists
	 */
	public function createList()
	{
		$list_style = $this->resultsWrap();
		$lists = '';
		$lists .= '<' . $list_style['heading_tag'] . '>';
		$lists .= $list_style['heading_text'];
		$lists .= '</' . $list_style['heading_tag'] .'>';
		$post_ids = parent::getPosts();
		if (empty($post_ids)) {
			$lists .= '<p class="no-results">';
			$lists .= apply_filters('no_results', __('Không có kết quả tìm kiếm.'));
			$lists .= '</p>';
			return $lists;
		}
		$lists .= '<ul class="list-results">';
		foreach ($post_ids as $id) {
			$post_title = get_the_title($id);
			$post_url = get_permalink($id);
			$img = wp_get_attachment_url(get_post_thumbnail_id($id));
			$meta_price = get_post_meta($id)['_price'];
            $min_price = min($meta_price);
            $max_price = max($meta_price);
            if($max_price == $min_price) {
            	$price_result = wc_price($min_price);
            } else {
            	$price_result = wc_price($min_price) . ' - ' . wc_price($max_price);
            }
			$lists .= '<li class="post-row">';
			$lists .= '<a class="post-title" href="' . $post_url . '">';
			$lists .= '<i class=""><img class="search_img_result" src='.$img.' /></i> <h3 class="title">' . $post_title . '<h3><span class="price">Giá '.$price_result.'</span></h3></a></li>';
		}
		$lists .= '</ul>';
		return $lists;
	}
}
